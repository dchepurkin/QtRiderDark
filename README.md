# Install

## Windows
`xcopy QtRiderDark.xml %APPDATA%\QtProject\qtcreator\styles`

## MacOS
`cp QtRiderDark.xml ~/.config/QtProject/qtcreator/styles/`

## Linux
`cp QtRiderDark.xml ~/.config/QtProject/qtcreator/styles/`

